var $body = $('body'),
    $descricao = $('#DESCRICAO'),
    $cod = $('#COD_CATEGORIA'),
    $grupoNome = $("#grupoNome"),
    $labelCategoria = $("#labelCategoria");

$(function(){
    $body.on('submit', '.formCategoria', function(_e) {
        _e.preventDefault();
        salvarCategoria();
    });
});

function salvarCategoria() {
	if(!valida())
		return false;

	var oData = new Object();
	oData.DESCRICAO = $descricao.val();
	oData.COD_CATEGORIA = $cod.val();

	$.ajax({
        type: "POST",
        url: "../../categoria/salvar",
        dataType: "json",
        data: oData,
        success: function(oData) {
        	 if(oData.success) {
        		 alert(oData.msg);
        		 window.location.assign("/categoria");
        	 }
        }
    });
}

function excluirCategoria(id, nome) {
	var result = confirm('Deseja Realmente Excluir o Registro: ' + nome);
	
	if(result) {
		var oData = new Object();
		
		oData.COD_CATEGORIA = id;
		
		$.ajax({
	        type: "POST",
	        url: "../../categoria/excluir",
	        dataType: "json",
	        data: oData,
	        success: function(oData){
	        	 if(oData.success){
	        		 alert(oData.msg);
	        		 location.reload();
	        	 } else {
                     alert(oData.msg);
                 }
	        }
	    });
	}
}

function valida() {
	var retorno = true;
    $grupoNome.removeClass("has-error");
	$("#labelCategoria").remove();

	if(!$descricao.val()) {
        $grupoNome.addClass("has-error");
        $grupoNome.append("<label id=\"labelCategoria\" class=\"control-label\">Preencher o Nome da Categoria!</label>");
		retorno =  false;
	}

	return retorno;
}