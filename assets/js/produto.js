var $body = $('body'),
    $descricao = $('#DESCRICAO'),
    $cod = $('#COD_PRODUTO'),
    $codCat = $('#COD_CATEGORIA'),
    $grupoNome = $("#grupoNome"),
    $grupoCategoria = $("#grupoCategoria"),
    $labelProduto = $("#labelProduto"),
    $labelCategoria = $("#labelCategoria");

$(function(){
    $body.on('submit', '.formProduto', function(_e) {
        _e.preventDefault();
        salvarProduto();
    });
});

function salvarProduto() {
	if(!valida())
		return false;
	
	var oData = new Object();
	oData.DESCRICAO = $descricao.val();
    oData.COD_PRODUTO = $cod.val();
    oData.COD_CATEGORIA = $codCat.val();

	$.ajax({
        type: "POST",
        url: "../../produto/salvar",
        dataType: "json",
        data: oData,
        success: function(oData){
        	 if(oData.success){
        		 alert(oData.msg);
        		 window.location.assign("/produto");
        	 }
        }
    });
}

function excluirProduto(id, nome) {
	var result = confirm('Deseja Realmente Excluir o Registro: ' + nome);
	
	if(result) {
		var oData = new Object();
		
		oData.COD_PRODUTO = id;
		
		$.ajax({
	        type: "POST",
	        url: "../../produto/excluir",
	        dataType: "json",
	        data: oData,
	        success: function(oData){
	        	 if(oData.success){
	        		 alert(oData.msg);
	        		 location.reload();
	        	 }
	        }
	    });
	}
}

function valida() {
	var retorno = true;
	$grupoNome.removeClass("has-error");
	$labelProduto.remove();
    $grupoCategoria.removeClass("has-error");
	$labelCategoria.remove();
	
	if(!$descricao.val()) {
		$grupoNome.addClass("has-error");
		$grupoNome.append("<label id=\"labelProduto\" class=\"control-label\">Preencher o Nome do Produto!</label>");
		retorno =  false;
	}
	
	if(!$codCat.val()) {
        $grupoCategoria.addClass("has-error");
        $grupoCategoria.append("<label id=\"labelCategoria\" class=\"control-label\">Selecione a Categoria do Produto!</label>");
		retorno =  false;
	}
	
	return retorno;
}