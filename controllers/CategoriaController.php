<?php
require_once('models/CategoriaModel.php');

class CategoriaController extends Controller
{
    public function index()
    {
        $categoriaModel = new CategoriaModel();
        $dados['categorias'] = $categoriaModel->listarCategorias();
        return $this->view('categoriasList', $dados);
    }
    
    public function incluir()
    {
        return $this->view('categoria');
    }
    
    public function alterar($id = null)
    {
        try {
            if(!$id)
                throw new Exception('Envio de Parâmetro obrigatório!');

            $categoriaModel = new CategoriaModel();
            $dados['categoria'] = $categoriaModel->buscarCategoriaPorId($id);
            
            return $this->view('categoria', $dados);
        } catch(Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }
    
    public function salvar()
    {
        try {
            $cod = isset($_POST['COD_CATEGORIA']) ? $_POST['COD_CATEGORIA'] : 0;
            $descricao = isset($_POST['DESCRICAO']) ? $_POST['DESCRICAO'] : '';

            if(!$descricao)
                throw new Exception('Nome da Categoria Obrigatório');

            $categoriaModel = new CategoriaModel();
            
            if($cod)
                $retorno = $categoriaModel->atualizarCategoria($cod, $descricao);
            else
                $retorno = $categoriaModel->salvarCategoria($descricao);
            
            if(is_string($retorno))
                throw new Exception($retorno);
            
            if($retorno['linhasAfetadas'] > 0 && $retorno['nID'] == 0)
                $msg = 'Registro Alterado com Sucesso!';
            else if($retorno['linhasAfetadas'] > 0 && $retorno['nID'] > 0)
                $msg = 'Registro Incluído com Sucesso!';
            else
                $msg = 'Erro ao Incluir ou Alterar!';
            
            echo json_encode(array('success' => true, 'msg' => $msg, 'dados' => array()));
        } catch(Exception $e) {
            echo json_encode(array('success' => false, 'msg' => $e->getMessage(), 'dados' => array()));
        }
    }

    public function excluir()
    {
        try
        {
            $cod = isset($_POST['COD_CATEGORIA']) ? $_POST['COD_CATEGORIA'] : '';
            
            $categoriaModel = new CategoriaModel();
            $retorno = $categoriaModel->excluirCategoria($cod);
            
            if(is_string($retorno))
                throw new Exception($retorno);
            
            if($retorno['linhasAfetadas'] > 0)
                $msg = 'Registro Excluído com Sucesso!';
            else
                $msg = 'Nenhum Registro Afetado';

            echo json_encode(array('success' => true, 'msg' => $msg, 'dados' => array()));
        } catch(Exception $e) {
            echo json_encode(array('success' => false, 'msg' => $e->getMessage(), 'dados' => array()));
        }
    }
}