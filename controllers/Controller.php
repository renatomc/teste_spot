<?php

class Controller
{
    protected function view($phtml, $dados = null)
    {
        try {
            if(!is_array($dados) && $dados != null)
                throw new Exception('Os parâmetros devem ser enviados em forma de Array!');
            else if(isset($dados) && count($dados) > 0)
                foreach($dados as $k=>$v)
                {
                    $$k = $v;
                }

            require('views/'.$phtml.'.phtml');
        } catch(Exception $e) {
            echo $e->getMessage();
        }
    }
}