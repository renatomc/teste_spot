<?php
require_once('models/ProdutoModel.php');
require_once('models/CategoriaModel.php');

class ProdutoController extends Controller
{
    public function index()
    {
        $produtoModel = new ProdutoModel();
        $dados['produtos'] = $produtoModel->listarProdutos();
        return $this->view('produtosList', $dados);
    }
    
    public function incluir()
    {
        $categoriaModel = new CategoriaModel();
        $dados['categorias'] = $categoriaModel->listarCategorias();
        return $this->view('produto', $dados);
    }
    
    public function alterar($id = null)
    {
        try {
            if(!$id)
                throw new Exception('Envio de Parâmetro obrigatório!');

            $produtoModel = new ProdutoModel();
            $dados['produto'] = $produtoModel->buscarProdutoPorId($id);

            $categoriaModel = new CategoriaModel();
            $dados['categorias'] = $categoriaModel->listarCategorias();

            return $this->view('produto', $dados);
        } catch(Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }
    
    public function salvar()
    {
        try {
            $cod = isset($_POST['COD_PRODUTO']) ? $_POST['COD_PRODUTO'] : 0;
            $descricao = isset($_POST['DESCRICAO']) ? $_POST['DESCRICAO'] : '';
            $codCategoria = isset($_POST['COD_CATEGORIA']) ? $_POST['COD_CATEGORIA'] : 0;
            
            if(!$descricao)
                throw new Exception('Nome do Produto Obrigatório');
            if(!$codCategoria)
                throw new Exception('Categoria Obrigatória');
                
            $produtoModel = new ProdutoModel();
            
            if($cod)
                $retorno = $produtoModel->atualizarProduto($cod, $descricao, $codCategoria);
            else
                $retorno = $produtoModel->salvarProduto($descricao, $codCategoria);
            
            if(is_string($retorno))
                throw new Exception($retorno);
            
            if($retorno['linhasAfetadas'] > 0 && $retorno['nID'] == 0)
                $msg = 'Registro Alterado com Sucesso!';
            else if($retorno['linhasAfetadas'] > 0 && $retorno['nID'] > 0)
                $msg = 'Registro Incluído com Sucesso!';
            else
                $msg = 'Erro ao Incluir ou Alterar!';
            
            echo json_encode(array('success' => true, 'msg' => $msg, 'dados' => array()));
        } catch(Exception $e) {
            echo json_encode(array('success' => false, 'msg' => $e->getMessage(), 'dados' => array()));
        }
    }
    
    public function excluir()
    {
        try {
            $cod = isset($_POST['COD_PRODUTO']) ? $_POST['COD_PRODUTO'] : '';
            
            $produtoModel = new ProdutoModel();
            $retorno = $produtoModel->excluirProduto($cod);
            
            if(is_string($retorno))
                throw new Exception($retorno);
            
            if($retorno['linhasAfetadas'] > 0)
                $msg = 'Registro Excluído com Sucesso!';
            else
                $msg = 'Nenhum Registro Afetado';

            echo json_encode(array('success' => true, 'msg' => $msg, 'dados' => array()));
        } catch(Exception $e) {
            echo json_encode(array('success' => false, 'msg' => $e->getMessage(), 'dados' => array()));
        }
    }
}