<?php
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

$uri = $_SERVER['REQUEST_URI'];
$uri = urldecode(trim($uri,'/'));

$keys = explode('/', $uri);

$controller = (isset($keys[0]) && $keys[0] != '') ? $keys[0] : 'home';
$controller = ucfirst($controller).'Controller';
$action = isset($keys[1]) ? $keys[1] : 'index';
$param = isset($keys[2]) ? $keys[2] : '';

try
{
    require_once('controllers/controller.php');
    require_once('models/model.php');
    
    if(!file_exists('controllers/'.$controller.'.php'))
        throw new Exception('Controller inválido!');
    
    require_once('controllers/'.$controller.'.php');
        
    $obj = new $controller();
    if(!method_exists($controller , $action))
        throw new Exception('Método  inválido!');
    if($param)
        $obj->$action($param);
    else
        $obj->$action();
}
catch(Exception $e)
{
    echo $e->getMessage();
}