<?php

class CategoriaModel extends Model
{
    public function listarCategorias()
    {
        try {
            $db = parent::dbConect();
            
            $query  = 'SELECT ';
            $query .= '* ';
            $query .= 'from CATEGORIA ';

            $categorias = $this->select($db, $query);
            
            if(is_string($categorias))
                throw new Exception($categorias);
            return $categorias;
        } catch(Exception $e) {
            return $e->getMessage();
        }
    }
    
    public function excluirCategoria($cod)
    {
        try {
            if(!isset($cod))
                throw new Exception('Enviar o Id do Registro');
            
             $db = parent::dbConect();
             $db->beginTransaction();
             
             $query = 'DELETE FROM CATEGORIA';
             $where = 'WHERE COD_CATEGORIA = '.$cod;
            
             $result = $this->delete($db, $query, $where);
             
             if(is_string($result))
                 throw new Exception($result);
             
             if($result['linhasAfetadas'] > 0)
                 $db->commit();
             else
                 $db->rollBack();

             return $result;
        } catch(Exception $e) {
            return $e->getMessage();
        }
    }
    
    public function salvarCategoria($descricao)
    {
        try {
            if(!isset($descricao))
                throw new Exception('Enviar a Descrição da Categoria');

            $db = parent::dbConect();
            $db->beginTransaction();
            
            $query  = 'INSERT INTO CATEGORIA (DESCRICAO)';
            $query .= 'VALUES(\''.$descricao.'\')';

            $result = $this->insert($db, $query);
                       
            if(is_string($result))
                throw new Exception($result);
                
            if($result['linhasAfetadas'] > 0)
                $db->commit();
            else
                $db->rollBack();

            return $result;
        } catch(Exception $e) {
            return $e->getMessage();
        }
    }
    
    public function atualizarCategoria($cod, $descricao)
    {
        try {
            if(!isset($cod))
                throw new Exception('Enviar Código da Categoria');
            if(!isset($descricao))
                throw new Exception('Enviar a Descrição da Categoria');

            $db = parent::dbConect();
            $db->beginTransaction();
            
            $query  = 'UPDATE CATEGORIA ';
            $query .= 'SET DESCRICAO = \''.$descricao.'\' ';

            $where = 'WHERE COD_CATEGORIA = '.$cod;
            
            $result = $this->update($db, $query, $where);
            
            if(is_string($result))
                throw new Exception($result);
                
            if($result['linhasAfetadas'] > 0)
                $db->commit();
            else
                $db->rollBack();

            return $result;
        } catch(Exception $e) {
            return $e->getMessage();
        }
    }
    
    public function buscarCategoriaPorId($id)
    {
        try {
            if(!isset($id))
                throw new Exception('Enviar o ID da Categoria');
            
            $db = parent::dbConect();
            
            $query   = 'SELECT ';
            $query  .= 'COD_CATEGORIA, ';
            $query  .= 'DESCRICAO ';
            $query  .= 'FROM CATEGORIA ';
            $query  .= 'WHERE COD_CATEGORIA = '.$id;
            $categoria  = $this->select($db, $query);
            
            if(is_string($categoria))
                throw new Exception($categoria);
            
            return $categoria;
        } catch(Exception $e) {
            return $e->getMessage();
        }
    }
}
