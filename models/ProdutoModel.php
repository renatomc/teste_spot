<?php

class ProdutoModel extends Model
{
    public function listarProdutos()
    {
        try {
            $db = parent::dbConect();
            
            $query  = 'SELECT ';
            $query .= 'p.COD_PRODUTO as COD_PRODUTO, ';
            $query .= 'p.DESCRICAO as DESCRICAO, ';
            $query .= 'c.DESCRICAO as CATEGORIA ';
            $query .= 'from PRODUTO p ';
            $query .= 'inner join CATEGORIA c  on c.COD_CATEGORIA = p.COD_CATEGORIA';
            
            $produtos = $this->select($db, $query);
            
            if(is_string($produtos))
                throw new Exception($produtos);
            return $produtos;
        } catch(Exception $e) {
            return $e->getMessage();
        }
    }
    
    public function excluirProduto($cod)
    {
        try {
            if(!isset($cod))
                throw new Exception('Enviar o Id do Registro');
            
            $db = parent::dbConect();
            $db->beginTransaction();

            $query = 'DELETE FROM PRODUTO';
            $where = 'WHERE COD_PRODUTO = '.$cod;

            $result = $this->delete($db, $query, $where);
             
            if(is_string($result))
                throw new Exception($result);
             
            if($result['linhasAfetadas'] > 0)
                $db->commit();
            else
                $db->rollBack();

            return $result;
        } catch(Exception $e) {
            return $e->getMessage();
        }
    }
    
    public function salvarProduto($descricao, $codCategoria)
    {
        try {
            if(!isset($descricao))
                throw new Exception('Enviar a Descrição do Produto');
            if(!isset($codCategoria))
                throw new Exception('Enviar a Categoria do Produto');
            
            $db = parent::dbConect();
            $db->beginTransaction();

            $query  = 'INSERT INTO PRODUTO (DESCRICAO, COD_CATEGORIA)';
            $query .= 'VALUES(\''.$descricao.'\', \''.$codCategoria.'\')';

            $result = $this->insert($db, $query);
                       
            if(is_string($result))
                throw new Exception($result);
                
            if($result['linhasAfetadas'] > 0)
                $db->commit();
            else
                $db->rollBack();

            return $result;
        } catch(Exception $e) {
            return $e->getMessage();
        }
    }
    
    public function atualizarProduto($cod, $descricao, $codCategoria)
    {
        try {
            if(!isset($cod))
                throw new Exception('Enviar o Cod do Produto');
            if(!isset($descricao))
                throw new Exception('Enviar a Descrição do Produto');
            if(!isset($codCategoria))
                throw new Exception('Enviar a Categoria do Produto');
                    
            $db = parent::dbConect();
            $db->beginTransaction();
            
            $query  = 'UPDATE PRODUTO ';
            $query .= 'SET DESCRICAO = \''.$descricao.'\', ';
            $query .= 'COD_CATEGORIA = '.$codCategoria;
            
            $where = 'WHERE COD_PRODUTO = '.$cod;
            
            $result = $this->update($db, $query, $where);
            
            if(is_string($result))
                throw new Exception($result);
                
            if($result['linhasAfetadas'] > 0)
                $db->commit();
            else
                $db->rollBack();

            return $result;
        } catch(Exception $e) {
            return $e->getMessage();
        }
    }
    
    public function buscarProdutoPorId($id)
    {
        try {
            if(!isset($id))
                throw new Exception('Enviar o ID do Produto');
            
            $db = parent::dbConect();
            
            $query   = 'SELECT ';
            $query  .= 'COD_PRODUTO, ';
            $query  .= 'DESCRICAO, ';
            $query  .= 'COD_CATEGORIA ';
            $query  .= 'FROM PRODUTo ';
            $query  .= 'WHERE COD_PRODUTO = '.$id;
            $produtos  = $this->select($db, $query);
            
            if(is_string($produtos))
                throw new Exception($produtos);
            
            return $produtos;
        } catch(Exception $e) {
            return $e->getMessage();
        }
    }
}
