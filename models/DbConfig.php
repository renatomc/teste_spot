<?php

class DbConfig
{
    
    protected static $db;
    protected static $host = 'localhost';
    protected static $name = 'spot';
    protected static $user = '';
    protected static $pass = '';
    
    public function __construct()
    {
        self::$db = new PDO('sqlsrv:server='.self::$host.';Database='.self::$name.'', self::$user, self::$pass);
        self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    
    public static function dbConect()
    {
        if(!self::$db)
            new DbConfig();

        return self::$db;
    }
}