<?php

require_once 'DbConfig.php';

class Model extends DbConfig
{
    public function select($dbConect, $query)
    {
        try {
            $stmt = $dbConect->prepare($query);
            $stmt->execute();
            $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            if(is_string($retorno))
                throw new Exception($retorno);
            
            return $retorno;
        } catch(Exception $e) {
            return $e->getMessage();
        }
    }
    
    public function insert($dbConect, $query)
    {
        try {
            $stmt = $dbConect->prepare($query);
            $retorno = $stmt->execute();
            
            if(is_string($retorno))
                throw new Exception($retorno);
            
           $aRet['linhasAfetadas'] = $stmt->rowCount();
           $aRet['nID'] = $dbConect->lastInsertId();
            
           return $aRet;
        } catch(Exception $e) {
            return $e->getMessage();
        }
    }
    
    public function delete($dbConect, $query, $where)
    {
        try {
            if(!$where)
                throw new Exception('Cláusula Where Obrigatória');

            $stmt = $dbConect->prepare($query.' '.$where);
            $retorno = $stmt->execute();
            
            if(is_string($retorno))
                throw new Exception($retorno);
                
            $aRet['linhasAfetadas'] = $stmt->rowCount();
            $aRet['nID'] = $dbConect->lastInsertId();
                
            return $aRet;
        } catch(Exception $e) {
            return $e->getMessage();
        }
    }
    
    public function update($dbConect, $query, $where)
    {
        try {
            if(!$where)
                throw new Exception('Cláusula Where Obrigatória');
                
            $stmt = $dbConect->prepare($query.' '.$where);
            $retorno = $stmt->execute();
                
            if(is_string($retorno))
                throw new Exception($retorno);
                    
            $aRet['linhasAfetadas'] = $stmt->rowCount();
            $aRet['nID'] = $dbConect->lastInsertId();
            
            return $aRet;
        } catch(Exception $e) {
            return $e->getMessage();
        }
    }
}